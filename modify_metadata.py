# -*- coding: utf-8 -*-

import os
import zipfile
import shutil
import xml.etree.ElementTree as ET
import datetime
from pytz import timezone
import csv
from dotenv import load_dotenv
import sys
import logging
import json
import random
import re

class CloneFiles:
    def __init__(self):
        self.initLogger()

        arguments = sys.argv

        load_dotenv()
        self.TEMP_DIR = os.getenv('TEMP_DIR')
        self.DOCUMENTS_DIR = os.getenv('DOCUMENTS_DIR')
        self.OUTPUT_FOLDER = os.getenv('OUTPUT_FOLDER')
        self.FILE_LIST_JSON = os.getenv('FILE_LIST_JSON')
        
        if not os.path.exists(self.OUTPUT_FOLDER):
            os.makedirs(self.OUTPUT_FOLDER)

        if len(arguments) > 1:
            self.CSV_FILE = arguments[1]
        else:
            self.CSV_FILE = os.getenv('CSV_FILE')
        
        self.requiredFiles = {}
        self.buildRequiredFilesList()
        
        self.fileList = {}
        self.loadFileList()
        self.modifyFiles()       

        

    def initLogger(self):
        self.logger = logging.getLogger('fileCloner')
        self.logger.setLevel(logging.INFO)

        fileHandler = logging.FileHandler('import.log')
        fileHandler.setLevel(logging.INFO)

        fileErrorHandler = logging.FileHandler('import_error.log')
        fileErrorHandler.setLevel(logging.ERROR)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fileHandler.setFormatter(formatter)
        fileErrorHandler.setFormatter(formatter)

        self.logger.addHandler(fileHandler)
        self.logger.addHandler(fileErrorHandler)

    def loadFileList(self):
        if os.path.isfile(self.FILE_LIST_JSON):
            choice = self.queryYesNo('File list json exists. Do you want to load it?')
            if choice:
                with open(self.FILE_LIST_JSON, 'r') as fp:
                    self.fileList = json.load(fp)
                    self.logger.info('File list loaded successfully!')
                    return
        
        self.buildFileList()

    def buildRequiredFilesList(self):
        if not os.path.isfile(self.CSV_FILE):
            print('CSV file not found: ' + self.CSV_FILE)
            self.logger.error('CSV file not found: ' + self.CSV_FILE)
            exit()

        self.logger.info('Build required files list started')
        with open(self.CSV_FILE, encoding='utf8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if not row['Path'].strip():
                    continue

                row['Path'] = self.removeSpecialChars(row['Path'])
                row['CreatedAt_File'] = self.removeSpecialChars(row['CreatedAt_File'])
                row['ModifiedAt_File'] = self.removeSpecialChars(row['ModifiedAt_File'])

                # print(row['Path'])
                # print(row)
                self.requiredFiles[row['Path']] = row

        self.logger.info('Build required files list finished')
        


    def unzipDoc(self, filePath):
        zip = zipfile.ZipFile(filePath)
        zip.extractall(self.TEMP_DIR)


    def modifyMetaData(self, row, fileExtension):
        # print(row)
        # exit()
        xmlPath = os.path.join(self.TEMP_DIR, 'docProps', 'core.xml')
        ET.register_namespace('cp', 'http://schemas.openxmlformats.org/package/2006/metadata/core-properties')
        ET.register_namespace('dc', 'http://purl.org/dc/elements/1.1/')
        ET.register_namespace('dcterms', 'http://purl.org/dc/terms/')
        ET.register_namespace('dcmitype', 'http://purl.org/dc/dcmitype/')
        ET.register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        ET.register_namespace('vt', 'http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes')
        tree = ET.parse(xmlPath)

        if not row['Creator']:
            self.logger.warning('Creator not defined: ' + row['Path'])

        creator = tree.find('{http://purl.org/dc/elements/1.1/}creator')
        creator.text = row['Creator']

        if not row['User']:
            self.logger.warning('Last modifier not defined: ' + row['Path'])
            
        modifier = tree.find('{http://schemas.openxmlformats.org/package/2006/metadata/core-properties}lastModifiedBy')
        modifier.text = row['User']

        created = tree.find('{http://purl.org/dc/terms/}created')
        created.text = self.formatDate(row['CreatedAt_File'])
        modified= tree.find('{http://purl.org/dc/terms/}modified')
        modified.text = self.formatDate(row['ModifiedAt_File'])

        lastPrinted = tree.find('{http://schemas.openxmlformats.org/package/2006/metadata/core-properties}lastPrinted')
        if lastPrinted is not None:
            root = tree.getroot()
            root.remove(lastPrinted)

        if fileExtension == '.docx':
            revision = tree.find('{http://schemas.openxmlformats.org/package/2006/metadata/core-properties}revision')
            revision.text = ''

            appXmlPath = os.path.join(self.TEMP_DIR, 'docProps', 'app.xml')
            appTree = ET.parse(appXmlPath)

            pages = appTree.find('{http://schemas.openxmlformats.org/officeDocument/2006/extended-properties}Pages')
            
            if not row['TotalTime']:
                totalTimeMins  = str(round(int(pages.text) * 10 * random.uniform(0.8, 1.2)))
            else:
                totalTimeMins = row['TotalTime']

            totalTime = appTree.find('{http://schemas.openxmlformats.org/officeDocument/2006/extended-properties}TotalTime')
            totalTime.text = totalTimeMins
            appTree.write(appXmlPath, encoding='UTF-8', xml_declaration=True)

        tree.write(xmlPath, encoding='UTF-8', xml_declaration=True)


    def zipDocument(self, filePath):
        zf = zipfile.ZipFile(filePath, "w", zipfile.ZIP_DEFLATED)
        for dirname, subdirs, files in os.walk(self.TEMP_DIR):
            if dirname != self.TEMP_DIR:
                zf.write(dirname, dirname.replace(self.TEMP_DIR + os.sep, ''))

            for filename in files:
                absolutePath = os.path.join(dirname, filename)
                relativePath = absolutePath.replace(self.TEMP_DIR + os.sep, '')
                zf.write(absolutePath, relativePath)

        zf.close()


    def deleteTempDir(self):
        shutil.rmtree(self.TEMP_DIR, ignore_errors=True)


    def modifyFiles(self):
        if not os.path.isfile(self.CSV_FILE):
            print('CSV file not found: ' + self.CSV_FILE)
            self.logger.error('CSV file not found: ' + self.CSV_FILE)
            exit()

        for key, row in self.requiredFiles.items():
            if not row['Path']:
                continue

            filePath = self.fileList.get(row['Path'], False)
            if not filePath:
                self.logger.error('File not found: ' + row['Path'])
                continue

            filename, fileExtension = os.path.splitext(filePath)
            if fileExtension not in ['.docx', '.xlsx']:
                shutil.copyfile(filePath, os.path.join(self.OUTPUT_FOLDER, row['Path']))
                self.logger.info('File copied to output directory: ' + row['Path'])
                continue

            createdAt = self.formatDate(row['CreatedAt_File'])
            modifiedAt = self.formatDate(row['ModifiedAt_File'])
            if not createdAt or not modifiedAt:
                self.logger.error('Invalid date format for file: ' + row['Path'] + ' Cant modify metadata.')
                continue

            self.logger.info('Start modify: ' + row['Path'])
            self.unzipDoc(filePath)
            self.modifyMetaData(row, fileExtension)

            self.zipDocument(os.path.join(self.OUTPUT_FOLDER, row['Path']))
            self.deleteTempDir()
            self.logger.info('Modify success: ' + row['Path'])


    def formatDate(self, dateString):
        dateString = self.removeSpecialChars(dateString)

        try:
            dt = datetime.datetime.strptime(dateString, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            return False

        dt_utc = timezone('Europe/Budapest').localize(dt).astimezone(timezone('UTC'))
        return dt_utc.strftime('%Y-%m-%dT%H:%M:%SZ')


    def buildFileList(self):
        print('File list building started')
        self.logger.info('File list building started')
        for root, directories, filenames in os.walk(self.DOCUMENTS_DIR):
            for originalFilename in filenames:
                filename = self.removeSpecialChars(originalFilename)
                if filename in self.requiredFiles:
                    self.fileList[filename] = os.path.join(root, originalFilename)

        with open(self.FILE_LIST_JSON, 'w') as fp:
            json.dump(self.fileList, fp, sort_keys=True, indent=4)

        print('File list building finished')
        self.logger.info('File list building finished')

    def removeSpecialChars(self, string):
        string = string.replace('\u200e', '')
        string = string.replace('\u200f', '')
        string = string.replace(u'\ufeff', '')

        string = string.strip()

        return string;


    def queryYesNo(self, question, default="yes"):
        valid = {"yes": True, "y": True, "ye": True,
                "no": False, "n": False}
        if default is None:
            prompt = " [y/n] "
        elif default == "yes":
            prompt = " [Y/n] "
        elif default == "no":
            prompt = " [y/N] "
        else:
            raise ValueError("invalid default answer: '%s'" % default)

        while True:
            sys.stdout.write(question + prompt)
            choice = input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'yes' or 'no' "
                                "(or 'y' or 'n').\n")
            

cloneFiles = CloneFiles()
